module github.com/yindaheng98/isglb

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/pion/ion v1.10.0
	github.com/pion/ion-log v1.2.2
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
